%global _empty_manifest_terminate_build 0
Name:		python-fasteners
Version:	0.14.1
Release:	1
Summary:	A python package that provides useful locks.
License:	ASL 2.0
URL:		https://github.com/harlowja/fasteners
Source0:	https://files.pythonhosted.org/packages/f4/6f/41b835c9bf69b03615630f8a6f6d45dafbec95eb4e2bb816638f043552b2/fasteners-0.14.1.tar.gz
BuildArch:	noarch
%description
Fasteners
=========

.. image:: https://travis-ci.org/harlowja/fasteners.png?branch=master
   :target: https://travis-ci.org/harlowja/fasteners

.. image:: https://readthedocs.org/projects/fasteners/badge/?version=latest
   :target: https://readthedocs.org/projects/fasteners/?badge=latest
   :alt: Documentation Status

.. image:: https://img.shields.io/pypi/dm/fasteners.svg
   :target: https://pypi.python.org/pypi/fasteners/
   :alt: Downloads

.. image:: https://img.shields.io/pypi/v/fasteners.svg
    :target: https://pypi.python.org/pypi/fasteners/
    :alt: Latest Version

Overview
--------

A python `package`_ that provides useful locks.

It includes the following.

Locking decorator
*****************

* Helpful ``locked`` decorator (that acquires instance
  objects lock(s) and acquires on method entry and
  releases on method exit).

Reader-writer locks
*******************

* Multiple readers (at the same time).
* Single writers (blocking any readers).
* Helpful ``read_locked`` and ``write_locked`` decorators.

Inter-process locks
*******************

* Single writer using file based locking (these automatically
  release on process exit, even if ``__release__`` or
  ``__exit__`` is never called).
* Helpful ``interprocess_locked`` decorator.

Generic helpers
***************

* A ``try_lock`` helper context manager that will attempt to
  acquire a given lock and provide back whether the attempt
  passed or failed (if it passes, then further code in the
  context manager will be ran **with** the lock acquired).

.. _package: https://pypi.python.org/pypi/fasteners

%package -n python2-fasteners
Summary:	A python package that provides useful locks.
Provides:	python-fasteners
BuildRequires:	python2-devel
BuildRequires:	python2-setuptools
Requires:	python2-six
Requires:	python2-monotonic
%description -n python2-fasteners
Fasteners
=========

.. image:: https://travis-ci.org/harlowja/fasteners.png?branch=master
   :target: https://travis-ci.org/harlowja/fasteners

.. image:: https://readthedocs.org/projects/fasteners/badge/?version=latest
   :target: https://readthedocs.org/projects/fasteners/?badge=latest
   :alt: Documentation Status

.. image:: https://img.shields.io/pypi/dm/fasteners.svg
   :target: https://pypi.python.org/pypi/fasteners/
   :alt: Downloads

.. image:: https://img.shields.io/pypi/v/fasteners.svg
    :target: https://pypi.python.org/pypi/fasteners/
    :alt: Latest Version

Overview
--------

A python `package`_ that provides useful locks.

It includes the following.

Locking decorator
*****************

* Helpful ``locked`` decorator (that acquires instance
  objects lock(s) and acquires on method entry and
  releases on method exit).

Reader-writer locks
*******************

* Multiple readers (at the same time).
* Single writers (blocking any readers).
* Helpful ``read_locked`` and ``write_locked`` decorators.

Inter-process locks
*******************

* Single writer using file based locking (these automatically
  release on process exit, even if ``__release__`` or
  ``__exit__`` is never called).
* Helpful ``interprocess_locked`` decorator.

Generic helpers
***************

* A ``try_lock`` helper context manager that will attempt to
  acquire a given lock and provide back whether the attempt
  passed or failed (if it passes, then further code in the
  context manager will be ran **with** the lock acquired).

.. _package: https://pypi.python.org/pypi/fasteners

%package help
Summary:	Development documents and examples for fasteners
Provides:	python2-fasteners-doc
%description help
Fasteners
=========

.. image:: https://travis-ci.org/harlowja/fasteners.png?branch=master
   :target: https://travis-ci.org/harlowja/fasteners

.. image:: https://readthedocs.org/projects/fasteners/badge/?version=latest
   :target: https://readthedocs.org/projects/fasteners/?badge=latest
   :alt: Documentation Status

.. image:: https://img.shields.io/pypi/dm/fasteners.svg
   :target: https://pypi.python.org/pypi/fasteners/
   :alt: Downloads

.. image:: https://img.shields.io/pypi/v/fasteners.svg
    :target: https://pypi.python.org/pypi/fasteners/
    :alt: Latest Version

Overview
--------

A python `package`_ that provides useful locks.

It includes the following.

Locking decorator
*****************

* Helpful ``locked`` decorator (that acquires instance
  objects lock(s) and acquires on method entry and
  releases on method exit).

Reader-writer locks
*******************

* Multiple readers (at the same time).
* Single writers (blocking any readers).
* Helpful ``read_locked`` and ``write_locked`` decorators.

Inter-process locks
*******************

* Single writer using file based locking (these automatically
  release on process exit, even if ``__release__`` or
  ``__exit__`` is never called).
* Helpful ``interprocess_locked`` decorator.

Generic helpers
***************

* A ``try_lock`` helper context manager that will attempt to
  acquire a given lock and provide back whether the attempt
  passed or failed (if it passes, then further code in the
  context manager will be ran **with** the lock acquired).

.. _package: https://pypi.python.org/pypi/fasteners

%prep
%autosetup -n fasteners-0.14.1

%build
%py2_build

%install
%py2_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python2-fasteners -f filelist.lst
%dir %{python2_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Apr 30 2021 OpenStack_SIG <openstack@openeuler.org>
- Package Spec generated
